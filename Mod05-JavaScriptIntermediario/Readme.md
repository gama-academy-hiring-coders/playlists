# Módulo 05 - JavaScript Intermediário

| Módulo | Curso                                                                                                                       | Aula                                | Duração | Situação |
| ------ | --------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ------- | -------- |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) | Template String                     | 09:31   | ❌        |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) | Operador Ternário e Arrow Functions |         | ❌        |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) | Spread                              |         | ❌        |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) | Desestruct e Match                  |         | ❌        |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) | SPA, PWA e WebComponents            |         | ❌        |