# Módulo 03 - GIT

| Módulo | Curso                                                                             | Aula                                  | Duração | Situação |
| ------ | --------------------------------------------------------------------------------- | ------------------------------------- | ------- | -------- |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Versionamento: Principais Ferramentas | 03:47   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Init Add Commit                       | 07:57   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Log, Status e Show                    | 11:23   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Branch, Checkout, Merge e Push        | 09:51   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Clone e Pull                          | 10:58   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Ignore                                | 03:12   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Pull Request                          | 09:26   | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md) | Gitflow                               | 06:15   | ✔️        |

## **Init** 
- Inicia o versionamento no diretório
```bash
$ git init  
```
## **Add e Staging** 
- Adiciona ou modifica alterações elegíveis para nosso ponto no tempo
```bash
$ git add index.html 
```
## **Commit**
- Gera um ponto na linha do tempo para noisso arquivo
```bash
$ git commit -m "adicionando arquivo index do nosso sistema"
```
## **Log**
- Visualiza os pontos na linha do tempo(commits)
```bash
$ git log
```
## **Status**
- Informa estado atual de alterações
```bash
$ git status
```

## **Branch**
### Criação de uma branch
```bash
// Cria uma branch
$ git branch <nome da branch>

// Migra para o branch ( agora vc esta trabalhando na branch )
$ git checkout feature/lista-procutos
```
### Exclusão de uma branch
```bash
$ git branch -D <nome da branch>
```
## Reset
- Desfaz a ultima alteração feita
```bash
$ git reset
```

# Merge
- Para fazer o merge é necessário acessar a o repositorio que vai receber o merge e executar o comando abaixo para fazer a união das alterações
```bash
$ git merge / feature/lista-produtos
```

# Show
- Apresenta um ponto indicado na linha do tempo
```bash
$ git show <id do commit>
```