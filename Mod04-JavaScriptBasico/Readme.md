# Módulo 04 - JavaScript Básico


| Módulo | Curso                                                                                                         | Aula                          | Duração | Situação |
| ------ | ------------------------------------------------------------------------------------------------------------- | ----------------------------- | ------- | -------- |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Introdução ao Javascript      | 06:26   | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Números e Operações           |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Boas Práticas de JavaScript   |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Condicionais                  |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Estrutura de Repetição: For   |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Estrutura de Repetição: While |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Funções                       |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Classes                       |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Validador de CPF I            |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Validador de CPF II           |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Validador de CPF III          |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Validador de CPF IV           |         | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_basico.md) | Validador de CPF V            |         | ✔️        |

## Projeto
* [Validador de CPF]()