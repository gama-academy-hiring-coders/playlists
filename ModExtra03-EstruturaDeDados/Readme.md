# Módulo Extra 03 - Estrutura de Dados

| Módulo | Curso                                                                                                           | Aula                        | Duração | Situação |
| ------ | --------------------------------------------------------------------------------------------------------------- | --------------------------- | ------- | -------- |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Introdução                  | 03:49   | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Pilhas I                    |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Pilhas Ii                   |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Filas e Listas              |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Busca I                     |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Busca II                    |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Algorítimos de Ordenação I  |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Algorítimos de Ordenação Ii |         | ❌        |