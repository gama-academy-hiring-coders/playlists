<h1> Lógica de Programação </h1>

<h2>O que é um algoritmo ?</h2>

* **ALGORITMO** é uma sequência finita de passos ( instruções / comandos ) ordenados, cujo o objetivo é a **RESOLUÇÃO DE UM PROBLEMA** através da **TRANSFORMAÇÃO DA INFORMAÇÃO** (DADOS)
* Transforma uma informação
* Tem uma sequência finita de passos
* É Ordenada 
* Resolve um PROBLEMA


<h3>Padrão de Execução de um algoritmo : </h3>

<img src="./assets/entrada.png" />

* ENTRADA : Toda informação que eu forneço para o computador;
* PROCESSAMENTO : É a sequencia de passos para reolver o problema;
* SAÍDA : É o Resultado;

<h2>Variáveis e Operações</h2>

<h3>O que é uma variável ?</h3>

* Ela armazena informação;
* Reserva espaço na memória para armazenar informação relevante;
* Armazenar informação;
* Pode ser usada para Cálculos aritméticos; 
* Pode ser usada para operações Relacionais;

<h2>Decisões</h2>

<div align="center"> 
  <img src="./assets/decisao01.png" />
  </br>
  <img src="./assets/decmultiplas.png" />
</div>

<h2>Repetições</h2>

<div align="center"> 
  <img src="./assets/repeticao.png" />
  </br>
</div>

