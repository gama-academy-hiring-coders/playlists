
# Playlists / Módulos

| Módulo | Curso                                                                                                                       | Duração | Situação |
| ------ | --------------------------------------------------------------------------------------------------------------------------- | ------- | -------- |
| 01     | Introdução                                                                                                                  |         | ✔️        |
| 02     | [Lógica de Programação](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/logica_de_programacao.md)       |         | ✔️        |
| 03     | [GIT](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/git.md)                                           | 06:15   | ✔️        |
| 04     | [JavaScript Básico](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md)        | 09:31   | ✔️        |
| 05     | [JavaScript Intermediário](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md) |         | ❌        |
| 06     | [React](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md)                    |         | ❌        |
| 07     | [TypeScript](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md)               |         | ❌        |
| 08     | [Testes](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/javascript_intermediario.md)                   |         | ❌        |

## Extras

| Módulo | Curso                                                                                                           | Aula                                           | Duração | Situação |
| ------ | --------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ------- | -------- |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md)                             | Ferramentas e Estrutura                        |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md)                             | Tags                                           |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md)                             | Listas e Tabela                                |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md)                             | Divisões I                                     |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md)                             | Divisões II                                    |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Tag Style                                      |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Atributos                                      |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Flexbox                                        |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Classes, Tags e IDs                            |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Classes, Tags e IDs II                         |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md)                               | Animações em CSS                               |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Introdução                                     | 03:49   | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Pilhas I                                       |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Pilhas Ii                                      |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Filas e Listas                                 |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Busca I                                        |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Busca II                                       |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Algorítimos de Ordenação I                     |         | ❌        |
| EXTRA  | [Estrutura de Dados](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/estrutura_de_dados.md) | Algorítimos de Ordenação Ii                    |         | ❌        |
| EXTRA  | [Metodologias Ágeis](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/metodologias_ageis.md) | Scrum: O que é e para que serve                | 03:04   | ❌        |
| EXTRA  | [Metodologias Ágeis](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/metodologias_ageis.md) | Papéis                                         | 02:52   | ❌        |
| EXTRA  | [Metodologias Ágeis](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/metodologias_ageis.md) | Eventos                                        | 05:45   | ❌        |
| EXTRA  | [Metodologias Ágeis](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/metodologias_ageis.md) | Artefatos e Refinamento                        | 03:44   | ❌        |
| EXTRA  | [Metodologias Ágeis](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/metodologias_ageis.md) | Kanban e Extreme Programming                   | 05:52   | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | Introdução                                     |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | BEM (Block Element Modifier)                   |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | Design System                                  |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | Storybook                                      |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | Micro Frontends                                |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | MVC (Model View Controller)                    |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | MVVM (Model View ViewModel)                    |         | ❌        |
| EXTRA  | [Boas Práticas](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/boas_praticas.md)           | Design Patterns- Singleton, Strategy e Adapter |         | ❌        |


# Desafios 

| Semana | Desafio                                     | Data de Entrega  | Status |
| ------ | ------------------------------------------- | ---------------- | ------ |
| 01     | Self Pitch                                  | 27/06/2021 23:59 | ✔️      |
| 01     | Autoconhecimento                            | 04/07/2021 23:59 | ✔️      |
| 01     | Inteligência Emocional                      | 04/07/2021 23:59 | ✔️      |
| 01     | Aprender a Aprender                         | 04/07/2021 23:59 | ✔️      |
| 01     | Transição de Carreira                       | 04/07/2021 23:59 | ✔️      |
| 01     | Estilos de Aprendizado                      | 04/07/2021 23:59 | ✔️      |
| 01     | Plano de Carreira e Expectativas            | 04/07/2021 23:59 | ✔️      |
| 01     | Growth Mindset                              | 04/07/2021 23:59 | ✔️      |
| 01     | Vocação                                     | 04/07/2021 23:59 | ✔️      |
| 01     | Vídeo-Aulas Semana 1                        | 04/07/2021 23:59 | ✔️      |
| 01     | Fatos & Mitos sobre Diversidade e Inclusão  | 04/07/2021 23:59 | ✔️      |
| 01     | O Futuro do Trabalho                        | 04/07/2021 23:59 | ✔️      |
| 01     | Processo de Aprendizagem                    | 04/07/2021 23:59 | ✔️      |
| 02     | Live #1- VTEX                               | 04/07/2021 23:59 | ✔️      |
| 02     | [Prova Técnica - Módulos 1 ao 3 e Extras]() | 06/07/2021 23:59 | ✔️      |
| 02     | Gestão de Projetos Ágeis                    | 11/07/2021 23:59 | ✔️      |
| 02     | Liderança                                   | 11/07/2021 23:59 | ✔️      |
| 02     | Lean Startup                                | 11/07/2021 23:59 | ❌      |
| 02     | Revisitando Seu Propósito                   | 11/07/2021 23:59 | ❌      |
| 02     | Video-Aulas da Semana 2                     | 11/07/2021 23:59 | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |
|        |                                             |                  | ❌      |



<h2> [Bootstrap](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/bootstrap.md) </h2>

- [ ]  Bootstrap: O que é e para que serve
- [ ]  Introdução e NavBar
- [ ]  CSS no Bootstrap
- [ ]  Bootstrap Cards
- [ ]  Formulários e Modais
- [ ]  Layout

<h2> [React](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/react.md) </h2>
 
- [ ] Boas-Vindas 03:29
- [ ] Por que React?
- [ ] Preparando o Ambiente
- [ ] Fundamentos do React
- [ ] Consumindo dados de uma API
- [ ] Instalando e Configurando o react-router-dom
- [ ] Importando as rotas no componente App
- [ ] Organização, Boas-Práticas e Componentes de Estilo
- [ ] Salvando Dados no Storage
- [ ] Renderizando Repositórios e Programação Declarativa
- [ ] Link react-router-dom
- [ ] Tratamento de Erros e useHistory
- [ ] Conditional Rendering
- [ ] Daqui pra frente e Encerramento  

<h2> [TypeScript](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/typescript.md) </h2>
 
- [ ] Introdução 08:08
- [ ] Instalando e Usando Typescript
- [ ] Tipos Básicos
- [ ] Tipo Never
- [ ] Múltiplos Tipos com Union Types
- [ ] Type Alias
- [ ] Valores nulos ou opcionais
- [ ] Type Assertion
- [ ] Interfaces
- [ ] Classes
- [ ] Configurando o Target do Typescript
- [ ] Utilizando Modificadores de Acesso
- [ ] Herança 

<h2> [Testes](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/testes.md) </h2>

- [ ] Testes - O que testar, por que testar, como testar?
- [ ] Testes - Test Driven Development (TDD)
- [ ] Testes - Behavior Driven Development (BDD)
- [ ] Testes - Jasmine I
- [ ] Testes - Jasmine II
- [ ] Testes - Cucumber I
- [ ] Testes - Cucumber II
- [ ] Testes - Jest
- [ ] Testes - Cypress 


