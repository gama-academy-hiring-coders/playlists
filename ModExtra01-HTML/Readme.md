# Módulo Extra 01 - HTML

| Módulo | Curso                                                                               | Aula                    | Duração | Situação |
| ------ | ----------------------------------------------------------------------------------- | ----------------------- | ------- | -------- |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md) | Ferramentas e Estrutura |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md) | Tags                    |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md) | Listas e Tabela         |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md) | Divisões I              |         | ❌        |
| EXTRA  | [HTML](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/html.md) | Divisões II             |         | ❌        |
