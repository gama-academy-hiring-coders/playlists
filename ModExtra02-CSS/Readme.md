# Módulo Extra 02 - CSS

| Módulo | Curso                                                                             | Aula                   | Duração | Situação |
| ------ | --------------------------------------------------------------------------------- | ---------------------- | ------- | -------- |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Tag Style              |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Atributos              |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Flexbox                |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Classes, Tags e IDs    |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Classes, Tags e IDs II |         | ❌        |
| EXTRA  | [CSS](https://gitlab.com/gama-academy-hiring-coders/playlists/-/blob/main/css.md) | Animações em CSS       |         | ❌        |
